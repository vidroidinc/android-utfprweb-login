package com.moshekaplan.android.wifiautologin.activities;



import com.libel.android.wifiautologin.AboutActivity;
import com.libel.android.wifiautologin.AjudaActivity;
import com.moshekaplan.android.EasyHTTPClient;
import com.moshekaplan.android.wifiautologin.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;

import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;


public class EditConfiguration extends ActionBarActivity

{
	private EditText usernameField;
	private EditText passwordField;
	//private AdView mAdView;
	private WebView webViewTeste, webViewRedirecionamento;
	boolean loadingFinished = true;
	boolean redirect = false;
	public static String urlWithToken = "";


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_configuration);
		usernameField = (EditText) findViewById(R.id.username);
		passwordField = (EditText) findViewById(R.id.password);
		loadStoredUsernamePassword();
		checkNetworkConnection();
		getRedirectUrl();
		/*
		mAdView = (AdView) findViewById(R.id.adView);
		AdRequest adRequest = new AdRequest.Builder().build();
		mAdView.loadAd(adRequest);
		 */
	}

	@Override
	protected void onStart() {
		super.onStart();
		getRedirectUrl();
	}

	private void loadStoredUsernamePassword(){
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		String storedUsername = preferences.getString("username", "");
		String storedPassword = preferences.getString("password", "");
		
		//EditText usernameField = (EditText) findViewById(R.id.username);
		//EditText passwordField = (EditText) findViewById(R.id.password);
		
		usernameField.setText(storedUsername);
		passwordField.setText(storedPassword);
	}
	

	public void manual_logon(View v) {
    	
    	//EditText usernameField = (EditText) findViewById(R.id.username);
		//EditText passwordField = (EditText) findViewById(R.id.password);
		
		final String username = usernameField.getText().toString();
		final String password = passwordField.getText().toString();
		
		Log.d("WifiConnectedReceiver", "username: " + username + " password: " + password);
		new Thread(new Runnable() {
			@Override
			public void run() {
				EasyHTTPClient.connect(username, password);
				}
		}).start();
		
		Toast.makeText(this, R.string.sent, Toast.LENGTH_SHORT).show();

		checkNetworkConnection();

	}
	
	
	public void onSave(View v) {
		//EditText usernameField = (EditText) findViewById(R.id.username);
		//EditText passwordField = (EditText) findViewById(R.id.password);
		
		String username = usernameField.getText().toString();
		String password = passwordField.getText().toString();
		
		Editor preferencesEditor = PreferenceManager.getDefaultSharedPreferences(this).edit();
		preferencesEditor.putString("username", username);
		preferencesEditor.putString("password", password);
		
		preferencesEditor.commit();
		Toast.makeText(this, R.string.saved, Toast.LENGTH_SHORT).show();
		checkNetworkConnection();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.about_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

	    // Handle item selection
	    switch (item.getItemId()) {
	        case R.id.about:
	            displayAbout();
	            return true;
            case R.id.ajuda:{
                displayAjuda();
            }
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}

	
	private void displayAbout() {
		Intent intent = new Intent(this, AboutActivity.class);
		startActivity(intent);
		finish();
	}

	private void displayAjuda() {
		Intent intent = new Intent(this, AjudaActivity.class);
		startActivity(intent);
		finish();
	}

	public boolean hostAvailable(String host, int port) {
		try (Socket socket = new Socket(host, port)) {
			InetAddress inetAddress = InetAddress.getByName(host);
			socket.connect(new InetSocketAddress(inetAddress, port));
			return true;
		} catch (Exception e) {
			// Either we have a timeout or unreachable host or failed DNS lookup
			e.printStackTrace();
			return false;
		}
	}

	public void getRedirectUrl(){
		webViewRedirecionamento = (WebView) findViewById(R.id.webViewRedirecionamento);
		WebSettings webSettings = webViewRedirecionamento.getSettings();
		webSettings.setJavaScriptEnabled(true);
		webSettings.setAllowContentAccess(true);
		webSettings.setAllowFileAccess(true);
		webSettings.setAppCacheEnabled(true);
		webSettings.setDomStorageEnabled(true);
		webSettings.getAllowContentAccess();
		webSettings.getCacheMode();
		webViewRedirecionamento.loadUrl("http://portal.utfpr.edu.br");

		webViewRedirecionamento.setWebViewClient(new WebViewClient(){
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String urlNewString) {
				if (!loadingFinished) {
					redirect = true;
				}

				loadingFinished = false;
				urlWithToken = urlNewString;
				view.loadUrl(urlNewString);
				return true;
			}

			@Override
			public void onPageStarted(WebView view, String url, Bitmap facIcon) {
				loadingFinished = false;
			}

			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);

				try{
					ConnectivityManager connectivityManager =  (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
					if(connectivityManager.getActiveNetworkInfo().isConnectedOrConnecting()){
					}
				}catch (Exception e){
					e.printStackTrace();
				}

			}
		});
	}

	public void checkNetworkConnection(){

		webViewTeste = (WebView) findViewById(R.id.webViewTeste);
		WebSettings webSettings = webViewTeste.getSettings();
		webSettings.setJavaScriptEnabled(true);
		webSettings.setAllowContentAccess(true);
		webSettings.setAllowFileAccess(true);
		webSettings.setAppCacheEnabled(true);
		webSettings.setDomStorageEnabled(true);
		webSettings.getAllowContentAccess();
		webSettings.getCacheMode();
		webViewTeste.loadUrl("http://portal.utfpr.edu.br");

		webViewTeste.setWebViewClient(new WebViewClient(){
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String urlNewString) {
				if (!loadingFinished) {
					redirect = true;
				}

				loadingFinished = false;
				view.loadUrl(urlNewString);
				return true;
			}

			@Override
			public void onPageStarted(WebView view, String url, Bitmap facIcon) {
				loadingFinished = false;
			}

			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);

				try{
					ConnectivityManager connectivityManager =  (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
					if(connectivityManager.getActiveNetworkInfo().isConnectedOrConnecting()){
						TextView txtConected = (TextView) findViewById(R.id.textViewConexao);
						txtConected.setText(R.string.conectado);
						txtConected.setTextColor(getResources().getColor(R.color.cor_conectado));
						Drawable drawable = getDrawable(R.drawable.ic_wifi_conectado_verde);
						txtConected.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, null, drawable);
					}
				}catch (Exception e){
					e.printStackTrace();
				}

			}
		});


		if(webViewTeste.getProgress() > 90){
			TextView txtConected = (TextView) findViewById(R.id.textViewConexao);
			txtConected.setText(R.string.conectado);
			txtConected.setTextColor(getResources().getColor(R.color.cor_conectado));
			txtConected.setCompoundDrawables(null, null, null, getResources().getDrawable(R.drawable.ic_wifi_conectado_verde));
		}



		if(hostAvailable("http://portal.utfpr.edu.br", 80)){
			TextView txtConected = (TextView) findViewById(R.id.textViewConexao);
			txtConected.setText(R.string.conectado);
			txtConected.setTextColor(getResources().getColor(R.color.cor_conectado));
			txtConected.setCompoundDrawables(null, null, null, getResources().getDrawable(R.drawable.ic_wifi_conectado_verde));
		}
	}


}

